EXT := dylib
EXT := so
EXT := dylib

all: target/debug/libpcap_image_decode.$(EXT)
	g++ src/main_rust.cpp -L ./target/release/ -lpcap_image_decode -o run_rust
	g++ src/main_cpp.cpp -o run_cpp

target/release/libpcap_image_decode.$(EXT): src/lib.rs Cargo.toml
	cargo build --release

target/debug/libpcap_image_decode.$(EXT): src/lib.rs Cargo.toml
	cargo build

clean:
	rm -rf target
	rm -rf run
