#include <iostream>
#include "example.h"

int main() {
    int nr_of_exec = 100000000;

    for (int i = 0; i < nr_of_exec; i++)
    {
        (void)double_input(i);
    }

    std::cout << nr_of_exec << std::endl;

    return 0;
}
